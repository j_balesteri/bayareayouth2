'use strict';

angular.module('bayAreaYouth')
  .controller('MainCtrl', function ($scope) {
    $scope.jumbotron = {
        'bgImg': '/assets/images/team005.jpg',
        'h1': 'Register now',
        'lead': 'Summer registration is open',
        'txt': 'Dates: 6/01/15 to 7/01/15',
        'btnTxt': 'Register!',
        'btnLink': 'registration'
      };
    $scope.featureBlocks = [
      {
        'cap': 'About Us',
        'subCap': 'Learn about BAY Flag Football',
        'url': 'about',
        'bgImg': '/assets/images/team001.jpg'
      },
      {
        'cap': 'Register Now',
        'subCap': 'Individual and team registration',
        'url': 'registration',
        'bgImg': '/assets/images/team002.jpg'
      },
      {
        'cap': 'Contact Us',
        'subCap': 'We would love to hear from you',
        'url': 'contact',
        'bgImg': '/assets/images/team003.jpg'
      }
    ];
  });
