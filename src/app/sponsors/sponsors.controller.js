'use strict';

angular.module('bayAreaYouth')
  .controller('SponsorsCtrl', function ($scope) {
    $scope.jumbotron = {
      'bgImg': '/assets/images/partners.jpg'
    },
    $scope.copy = {
      'h1': 'Sponsors',
      'lead': 'Bay Area Youth Flag Football has partnered with select businesses and individuals to bring you additional resources than just football. Please take an opportunity to visit some of our sponsors links and show them some love.'
    },
    $scope.sections = [
      {
        'hdr': 'Section One',
        'paragraphs': {
          'p1': 'Lorem ipsum Eiusmod cupidatat ex qui adipisicing esse reprehenderit laboris proident amet et ex sed adipisicing qui dolor esse irure ad et incididunt incididunt anim exercitation Excepteur nostrud.',
          'p2': 'Lorem ipsum In velit Duis sunt exercitation labore sit labore officia veniam dolore elit laboris Ut pariatur ex veniam dolore mollit dolor velit officia enim amet quis velit do ullamco dolore do ea consequat do irure qui non ut occaecat reprehenderit velit dolor adipisicing ea aute aliqua in ut mollit quis qui sint reprehenderit deserunt est cillum eiusmod ut enim voluptate ex aliquip esse commodo elit dolor dolor exercitation eiusmod sunt ullamco exercitation minim quis dolore id Excepteur dolor Duis est dolor eiusmod aliquip Ut adipisicing commodo velit exercitation nostrud nulla dolore exercitation quis velit non Ut anim dolore nisi enim laboris in veniam esse nisi aute occaecat adipisicing consequat est Ut enim ex Duis pariatur sint non mollit sunt Duis dolore laboris nisi do occaecat voluptate veniam incididunt sunt culpa ut proident ut in occaecat minim proident do aliquip ex in ut anim qui aliquip magna laboris dolore labore nostrud culpa commodo ex sunt consectetur et esse sint ut sit ea consequat mollit in aute sint adipisicing sit laborum proident fugiat do deserunt sit sunt aliquip eu amet velit commodo sint do mollit ea ad aliqua consequat proident in aute in sunt dolor aliqua ullamco in incididunt dolor do dolor dolore officia et nostrud qui eu nostrud veniam anim adipisicing do amet pariatur ut ullamco nulla proident ea laborum.'
        }
      },
      {
        'hdr': 'Section Two',
        'paragraphs': {
          'p1': 'Lorem ipsum Irure ex pariatur tempor laboris nulla esse et et cupidatat eu tempor in cupidatat dolore ad.'
        }
      },
      {
        'hdr': 'Section Three',
        'paragraphs': {
          'p1': 'Lorem ipsum Irure ex pariatur tempor laboris nulla esse et et cupidatat eu tempor in cupidatat dolore ad.',
          'p2': 'Lorem ipsum Irure ex pariatur tempor laboris nulla esse et et cupidatat eu tempor in cupidatat dolore ad.'
        }
      }
    ];
  });
